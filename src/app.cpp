#include <windows.h>
#include <stdio.h>
#include "app.h"

#pragma comment(lib,"ws2_32.lib")

#define SERVER "127.0.0.1"
#define BUFLEN 512
#define PORT 8888

#define APP_NAME "ScoreTek"

#define MSG_READY "Ready for clients."
#define MSG_FINISHED "Finished. Press ESC to exit"
#define MSG_ERROR "Network issue. Press ESC to exit"
#define MSG_NETWORKERROR "Transmission error. Press ESC to exit."

unsigned long bytes_received = 0;

HWND hwndApp;

HANDLE network_worker, hFile = INVALID_HANDLE_VALUE, hLog = INVALID_HANDLE_VALUE;

DWORD worker_thread_id;

BOOL actively_networking = true;

TCHAR Filename[MAX_PATH], progress[MAX_PATH/2];

void AppLog (LPCSTR);

// Networking variables

WSADATA wsa;
SOCKET sock;
struct sockaddr_in si_other;
int s2, slen = sizeof(si_other);

HRESULT InitNetworking() {

	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0) return ERROR_NETWORK_ACCESS_DENIED;

	if ((sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == SOCKET_ERROR) return ERROR_NETWORK_ACCESS_DENIED;

	return NOERROR;

}

#ifdef SENDER

void OpenForBusiness() {

	sockaddr_in server;

	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(PORT);

	if (bind(sock, (struct sockaddr *)&server, sizeof(server)) == SOCKET_ERROR) {

		wsprintf(progress, "ERROR:OpenForBusiness's bind failed", GetLastError());

		AppLog(progress);

	}

	ResumeThread(network_worker);

}

DWORD WINAPI ThreadProc (LPVOID ignored) {

		INT slen = sizeof(si_other);

		CHAR payload[BUFLEN];

		DWORD bytes_read_from_disk;

		ULONG bytes_processed = 0;

		wsprintf(progress, MSG_READY);

		PostMessage(hwndApp, WM_USER, 0, 0);

		ZeroMemory(&payload[0], BUFLEN);

		if ((recvfrom(sock, payload, BUFLEN, 0, (struct sockaddr *) &si_other, &slen)) != SOCKET_ERROR) {

			wsprintf(progress, "Received packet from %s:%d", inet_ntoa(si_other.sin_addr), ntohs(si_other.sin_port));
			AppLog(progress);

			wsprintf(progress, "Data: %s", payload);
			AppLog(progress);

			wsprintf(progress, "Connected");

			PostMessage(hwndApp, WM_USER, 0, 0);
		
			while (actively_networking) {

				ZeroMemory(&payload[0], sizeof(payload));

				if (ReadFile(hFile, PVOID(&payload[0]), BUFLEN, &bytes_read_from_disk, NULL)) {

					wsprintf(progress, "Sending...");

					PostMessage(hwndApp, WM_USER, 0, 0);

					bytes_processed += bytes_read_from_disk;

					if (sendto(sock, payload, bytes_read_from_disk, 0, (struct sockaddr*) &si_other, slen) == SOCKET_ERROR) {

						wsprintf(progress, "sendto() failed with error code : %d" , WSAGetLastError());

						AppLog(progress);

					}

					if (!bytes_read_from_disk) {

						closesocket(sock);

						actively_networking = false;

						wsprintf(progress, MSG_FINISHED);

						PostMessage(hwndApp, WM_USER, 0, 0);

					}

				} else {

					wsprintf(progress, MSG_FINISHED);

					PostMessage(hwndApp, WM_USER, 0, 0);

					actively_networking = false;

					closesocket(sock);

				}

			}

			AppLog("Network thread exited.");

			ExitThread(0);

		} else {

			wsprintf(progress, MSG_ERROR);

			PostMessage(hwndApp, WM_USER, 0, 0);

			wsprintf(progress, "recvfrom() failed with error code : %d", WSAGetLastError());

			AppLog(progress);

			ExitThread(0);

		}

	}

#else

void ConnectToServer() {

	TCHAR scratch[0xBD];

	memset((char *)&si_other, 0, sizeof(si_other));

	ZeroMemory(&scratch[0], sizeof(scratch));

	GetWindowText(GetDlgItem(hwndApp, IDC_IPADDRESS), scratch, sizeof(scratch));

	si_other.sin_addr.S_un.S_addr = inet_addr(scratch);	// Server's IP

	si_other.sin_port = htons(PORT);

	si_other.sin_family = AF_INET;

	hFile = CreateFile(Filename, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, 0, NULL);

	ResumeThread(network_worker);

}

DWORD WINAPI ThreadProc (LPVOID ignored) {

	INT bytes_received_from_socket;

	DWORD written_to_disk;

	INT slen = sizeof(si_other);

	CHAR payload[BUFLEN];

	ULONG bytes_processed = 0;

	AppLog("Start @ networking thread starting");

	ZeroMemory(&payload[0], BUFLEN);

	strcpy_s(payload, "BDxdb");

	if (sendto(sock, payload, strlen(payload), 0, (struct sockaddr *) &si_other, slen) == SOCKET_ERROR) {

		wsprintf(progress, "sendto() failed with error code : %d", WSAGetLastError());

		AppLog(progress);

		actively_networking = false;

	}

	while (actively_networking) {

		ZeroMemory(&payload[0], BUFLEN);

		if ((bytes_received_from_socket = recvfrom(sock, payload, BUFLEN, 0, (struct sockaddr *)&si_other, &slen)) != SOCKET_ERROR) {

			WriteFile(hFile, PVOID(payload), bytes_received_from_socket, &written_to_disk, NULL);

			bytes_received += bytes_received_from_socket;

			if (bytes_received_from_socket) {

				wsprintf(progress, "Received %ld", bytes_received);

			}
			else {

				wsprintf(progress, MSG_FINISHED);

			}

			PostMessage(hwndApp, WM_USER, 0, 0);

		}
		else {

			wsprintf(progress, MSG_NETWORKERROR);

			PostMessage(hwndApp, WM_USER, 0, 0);

			actively_networking = false;

		}

	}

	AppLog("Network thread exited.");

	ExitThread(0);

}

#endif

INT_PTR DialogProc (HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam) {

	switch (msg) {

		case WM_INITDIALOG:

			hwndApp = hwnd;

#ifndef SENDER
			SetWindowText(GetDlgItem(hwnd, IDC_IPADDRESS), SERVER);
#endif

			return FALSE;

			break;

		case WM_COMMAND:

			if (BN_CLICKED == HIWORD(wParam)) {

				if (IDC_FILEPICKY == LOWORD(wParam)) {

#ifdef SENDER

					OPENFILENAME ofn;

					ZeroMemory(&ofn, sizeof(ofn));

					ofn.lStructSize = sizeof(ofn);
					ofn.hwndOwner = hwndApp;
					ofn.lpstrFilter = "All files\0*.*\0\0\0";
					ofn.lpstrFile = &Filename[0];
					ofn.nMaxFile = MAX_PATH;
					ofn.lpstrTitle = "Source file";
					ofn.Flags = OFN_DONTADDTORECENT|OFN_LONGNAMES|OFN_NONETWORKBUTTON|OFN_NOREADONLYRETURN|OFN_PATHMUSTEXIST;

					if (GetOpenFileName(&ofn)) {

						if (INVALID_HANDLE_VALUE != (hFile = CreateFile(Filename, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_FLAG_SEQUENTIAL_SCAN, NULL))) {

							EnableWindow(GetDlgItem(hwnd, IDC_FILEPICKY), FALSE);

							EnableWindow(GetDlgItem(hwnd, IDOK), TRUE);

						} else {

							MessageBox(hwndApp, APP_NAME, "Source file not found!", MB_ICONHAND);

						}

					}

#else

					OPENFILENAME ofn;

					ZeroMemory(&ofn, sizeof(ofn));

					ofn.lStructSize = sizeof(ofn);
					ofn.hwndOwner = hwndApp;
					ofn.lpstrFilter = "All files\0*.*\0\0\0";
					ofn.lpstrFile = &Filename[0];
					ofn.nMaxFile = MAX_PATH;
					ofn.lpstrTitle = "Save As...";
					ofn.Flags = OFN_DONTADDTORECENT | OFN_LONGNAMES | OFN_NONETWORKBUTTON | OFN_NOREADONLYRETURN | OFN_PATHMUSTEXIST;

					if (GetSaveFileName(&ofn)) {

						EnableWindow(GetDlgItem(hwnd, IDC_FILEPICKY), FALSE);

						EnableWindow(GetDlgItem(hwnd, IDOK), TRUE);

					}

#endif

				}
					
				if (IDOK == LOWORD(wParam)) {

#ifdef SENDER

					ShowWindow(GetDlgItem(hwnd, IDC_STATIC), SW_HIDE);

					ShowWindow(GetDlgItem(hwnd, IDC_FILEPICKY), SW_HIDE);

					ShowWindow(GetDlgItem(hwnd, IDOK), SW_HIDE);

					ShowWindow(GetDlgItem(hwnd, IDC_PROGRESS), SW_SHOW);

					OpenForBusiness();

#else

					ShowWindow(GetDlgItem(hwnd, IDC_STATIC), SW_HIDE);

					ShowWindow(GetDlgItem(hwnd, IDC_IPADDRESS), SW_HIDE);

					ShowWindow(GetDlgItem(hwnd, IDC_FILEPICKY), SW_HIDE);

					ShowWindow(GetDlgItem(hwnd, IDOK), SW_HIDE);

					ShowWindow(GetDlgItem(hwnd, IDC_PROGRESS), SW_SHOW);

					ConnectToServer();

#endif

				}

				if (IDCANCEL == LOWORD(wParam)) {

					actively_networking = false;

					if (INVALID_HANDLE_VALUE != hFile) CloseHandle(HANDLE(hFile));

					return EndDialog(hwnd, 0);

				}

			}

			return TRUE;

			break;

		case WM_USER:

			SetWindowText(GetDlgItem(hwndApp, IDC_PROGRESS), progress);

			break;

	}

	return false;

}

int CALLBACK WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {

	AppLog("App launched.");

	ZeroMemory(&Filename[0], sizeof(Filename));

	if (SUCCEEDED(InitNetworking())) {

		network_worker = CreateThread(NULL, 0, ThreadProc, NULL, CREATE_SUSPENDED, &worker_thread_id);

		DialogBox(NULL, MAKEINTRESOURCE(IDD_UX), GetDesktopWindow(), DialogProc);

		closesocket(sock);

		WSACleanup();

	} else {

		wsprintf(progress, "ERROR:InitNetworking (%0X)", WSAGetLastError());

		AppLog(progress);

	}

	if (INVALID_HANDLE_VALUE != hLog) CloseHandle(hLog);

	AppLog("App end.");

	return 0;

}

void AppLog (LPCSTR msg) {

	DWORD bytes_written;

	CHAR payload[MAX_PATH/2];

	bytes_written = wsprintf(payload, "%s\n", msg);

	if (INVALID_HANDLE_VALUE == hLog) {

		GetModuleFileName(NULL, &Filename[0], sizeof(Filename));

		lstrcat(Filename, ".log.txt");

		hLog = CreateFile(&Filename[0], GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, 0, NULL);

		OutputDebugString(Filename);

	}

	if (INVALID_HANDLE_VALUE != hLog) {

		WriteFile(hLog, PVOID(payload), bytes_written, &bytes_written, NULL);

	}

}